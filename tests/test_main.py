import unittest, os, sys, shutil

from random import choices
from string import ascii_lowercase

sys.path.append('../src')
import note
from note.skel import Db
from note.constants import DATA_DIR


class TestDbActions(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.database = Db(file_name='test.pickle')
        cls.temp_directory = '/tmp/note_testing/'
        os.mkdir(cls.temp_directory)
        cls.random_values_amount = 9
        cls.random_values = [''.join(choices(ascii_lowercase, k=9)) for i in range(cls.random_values_amount)]

    def test_1_db_directory(self):
        self.assertTrue(os.path.isdir(DATA_DIR), msg='Database directory cannot be found!')

    def test_2_append(self):
        description = 'test123'
        directory = os.path.join(self.temp_directory, description)
        os.mkdir(directory)
        self.database.append_data(description=description, directory=directory)
        self.assertEqual(self.database.read_pickle()[0].description, description, msg='Append to database failed!')

    def test_3_db_file(self):
        self.assertTrue(os.path.isfile(self.database.file_dir), msg='Database file cannot be found!')

    def test_4_append_multiple_values(self):
        for i in self.random_values:
            directory = os.path.join(self.temp_directory, i)
            os.mkdir(directory)
            self.database.append_data(description=i, directory=os.path.join(self.temp_directory, i))
        self.assertEqual(len(self.database.read_pickle()), self.random_values_amount + 1, msg='Appending multiple values failed!')

    def test_5_remove_id_value(self):
        self.database.remove_data(id=3)
        self.assertEqual(len(self.database.read_pickle()), self.random_values_amount, msg='Deleting id value failed!')

    def test_6_remove_abbr_value(self):
        data = self.database.read_pickle()[4]
        self.database.remove_data(abbr=data.abbr)
        self.assertEqual(len(self.database.read_pickle()), self.random_values_amount - 1, msg='Deleting abbr value failed!')

    def test_7_append_after_removal(self):
        description = 'test456'
        directory = os.path.join(self.temp_directory, description)
        os.mkdir(directory)
        self.database.append_data(description=description, directory=directory)
        self.assertEqual(self.database.read_pickle()[-1].description, description, msg='Append to database failed!')

    def test_8_print_values(self):
        self.database.print_data()

    @classmethod
    def tearDownClass(cls):
        if cls.database.__created_file__:
            os.remove(cls.database.file_dir)
        if note.__created_dir__:
            os.rmdir(DATA_DIR)
        shutil.rmtree(cls.temp_directory)

if __name__ == '__main__':
    unittest.main()
