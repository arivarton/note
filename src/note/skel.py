import pathlib
import pickle

from datetime import datetime
from os.path import join, isfile, basename, normpath
from random import choices

from .constants import DATA_DIR
from .exceptions import AlreadyExists, NoValueDefined, TooManyValuesDefined

class Db:
    def __init__(self, file_name='note.pickle'):
        self.__created_file__ = False
        self.file_name = file_name
        self.file_dir = join(DATA_DIR, self.file_name)
        self.incremented_id = 0

    def write_pickle(self, data):
        with open(self.file_dir, 'wb') as f:
            pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

    def read_pickle(self):
        try:
            with open(self.file_dir, 'rb') as f:
                return pickle.load(f)
        except FileNotFoundError:
            self.__created_file__ = True
            return []

    def append_data(self, **kwargs):
        f = self.read_pickle()
        self.incremented_id += 1
        f.append(File(id=self.incremented_id, **kwargs))
        self.write_pickle(f)

    def print_data(self):
        f = self.read_pickle()
        for i in f:
            print('\n%s - %s - %s:\nCreated: %s\n%s' % (i.id, i.name, i.abbr, i.created, i.directory))

    def remove_data(self, id=None, abbr=None):
        if not id and not abbr:
            raise NoValueDefined('Please input either id or an abbreviation to remove!')
        elif id and abbr:
            raise TooManyValuesDefined('Choose either id or abbreviation, not both!')
        elif id:
            f = self.read_pickle()
            for count, i in enumerate(f):
                if i.id == id:
                    del f[count]
                    break
        elif abbr:
            f = self.read_pickle()
            for count, i in enumerate(f):
                if i.abbr == abbr:
                    del f[count]
                    break
        self.write_pickle(f)


class File:
    def __init__(self, id=0, file_name='.notes', abbr=None,
                 description=None, directory=pathlib.Path().absolute()):
        self.id = id
        self.created = datetime.now()
        self.file_name = file_name
        self.directory = directory
        self.name = basename(normpath(self.directory))
        self.abbr = abbr or self.random_abbr()
        self.description = description
        self.absolute_file_name = join(directory, file_name)

        self.create_file()

    def __str__(self):
        return self.file_name

    def random_abbr(self):
        return ''.join(choices(self.name.replace(' ', ''), k=3)).lower()

    def create_file(self):
        if not isfile(self.absolute_file_name):
            notes = open(self.absolute_file_name, 'w+')
            notes.write('Created by notes.')
            notes.close()
        else:
            raise AlreadyExists('File already exists!')
