from sys import argv

from .args import parse

def run(args=argv[1:]):
    parser = parse(argv[1:])
    parser.func(parser)

if __name__ == '__main__':
    run()
