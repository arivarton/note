import os
from .constants import DATA_DIR
from .exceptions import error_handler

__version__ = '0.0.2'
__created_dir__ = False

# Check for data directory
if not os.path.isdir(DATA_DIR): 
    try:
        os.mkdir(DATA_DIR)
    except FileNotFoundError:
        error_handler('Required parent folders of <%s> not found!' % DATA_DIR)
    finally:
        __created_dir__ = True
