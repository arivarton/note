import pathlib
import os

from .skel import Db
from .exceptions import error_handler, AddToDBFailed, AlreadyExists

database = Db()

def add(args):
    try:
        database.append_data(description=args.description)
    except (AlreadyExists, AddToDBFailed) as err_msg:
        error_handler(err_msg)

def show(args):
    database.print_data()
