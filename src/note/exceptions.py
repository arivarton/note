from sys import exit

def error_handler(error_message, exit_on_error=True):
    print(error_message)
    if exit_on_error:
        exit(0)


class NoteError(Exception):
    pass


class AlreadyExists(NoteError):
    def __init__(self, message='Already exists!'):
        super().__init__(message)


class NoValueDefined(NoteError):
    def __init__(self, message='No valid value defined!'):
        super().__init__(message)


class TooManyValuesDefined(NoteError):
    def __init__(self, message='Too many values defined!'):
        super().__init__(message)


class AddToDBFailed(NoteError):
    def __init__(self, message='Adding data to database failed!'):
        super().__init__(message)
