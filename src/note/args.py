import argparse

from note import __version__
from .interactions import add, show

def parse(args):
    # Main parser
    main_parser = argparse.ArgumentParser(description='''Create notes using
                                                         the command line.''',
                                          epilog='''By arivarton''')
    main_parser.add_argument('-v', '--version', action='version', version=__version__,
                             help='Display current version.')

    # Sub parsers
    main_subparsers = main_parser.add_subparsers()
    add_parser = main_subparsers.add_parser('add', aliases=['a'],
                                            help='Add new note.')
    add_parser.add_argument('description', nargs='?', default=None, type=str)
    add_parser.set_defaults(func=add)

    show_parser = main_subparsers.add_parser('show', aliases=['s'],
                                            help='Show all notes.')
    show_parser.set_defaults(func=show)

    return main_parser.parse_args(args)
